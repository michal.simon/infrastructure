# Docker swarm deployment stages
Stages serve only for deployment scripts where define order.

## Stage 0
- must have services - database, queue
- all stacks have to be independent

## Stage 1
- core of data platform

## Stage 2
- visualizations

## Stage 3
- system services - backups, logs monitoring 
 
# Notes
- Inspired by [Raspbian stages](https://github.com/RPi-Distro/pi-gen#raspbian-stage-overview)
