-- granty pro db

-- Prerequisites:
--   * all databases already exists (_dataplatform_database, _permission_proxy_database)
--   * run this script on _dataplatform_database 


------------
-- GRANTS --
------------
DO $$declare
    x_sql varchar;

    x_revoke_old_users boolean;

    _output_gateway_user varchar(50);
    _output_gateway_pass varchar(50);
    _dataplatform_database varchar(50);

    _integration_engine_user varchar(50);
    _integration_engine_pass varchar(50);

    _permission_proxy_user varchar(50);
    _permission_proxy_pass varchar(50);
    _permission_proxy_database varchar(50);

BEGIN
    x_revoke_old_users := false;

    _output_gateway_user := 'output_gateway1';
    _output_gateway_pass := 'DontForgetChangeMe';
    _dataplatform_database := 'dataplatform';

    _integration_engine_user:='integration_engine';
    _integration_engine_pass:='DontForgetChangeMe';

    _permission_proxy_user:='permission_proxy';
    _permission_proxy_pass:='DontForgetChangeMe';
    _permission_proxy_database:='permission_proxy';

-- create meta schema on dataplatform database
    execute 'SET SEARCH_PATH = "'||_dataplatform_database||'"';
    execute 'CREATE SCHEMA IF NOT EXISTS meta';


    IF x_revoke_old_users THEN
-- drop _output_gateway_user
        execute 'REVOKE all ON ALL TABLES IN SCHEMA public, meta FROM "'||_output_gateway_user||'"';
		execute 'REVOKE all ON SCHEMA meta FROM "'||_output_gateway_user||'"';
        execute 'REVOKE CONNECT ON DATABASE "'||_dataplatform_database||'" FROM "'||_output_gateway_user||'"';
        execute 'DROP USER "'||_output_gateway_user||'"';

-- drop _integration_engine_user
		execute 'REVOKE all ON SCHEMA meta FROM "'||_integration_engine_user||'"';
        execute 'REVOKE all ON ALL TABLES IN SCHEMA public, meta FROM "'||_integration_engine_user||'"';
		execute 'REVOKE all ON SCHEMA tmp FROM "'||_integration_engine_user||'"';
        execute 'REVOKE CONNECT ON DATABASE "'||_dataplatform_database||'" FROM "'||_integration_engine_user||'"';
        execute 'REVOKE EXECUTE ON ALL FUNCTIONS IN SCHEMA meta FROM "'||_integration_engine_user||'"';
        execute 'DROP USER "'||_integration_engine_user||'"';

-- drop _permission_proxy_user
		execute 'REVOKE all ON ALL TABLES IN SCHEMA public FROM "'||_permission_proxy_user||'"';
        execute 'REVOKE CONNECT ON DATABASE "'||_permission_proxy_database||'" FROM "'||_permission_proxy_user||'"';
        execute 'DROP USER "'||_permission_proxy_user||'"';
        
    END IF;


-- create and set user for output gateway
    execute 'CREATE USER "'||_output_gateway_user||'" WITH PASSWORD '''||_output_gateway_pass||'''';
    execute 'GRANT CONNECT ON DATABASE "'||_dataplatform_database||'" TO "'||_output_gateway_user||'"';
    execute 'SET SEARCH_PATH = "'||_dataplatform_database||'"';
    execute 'GRANT USAGE ON SCHEMA meta TO "'||_output_gateway_user||'"';
    execute 'GRANT SELECT  ON ALL TABLES IN SCHEMA public, meta TO "'||_output_gateway_user||'"';

-- create and set user for integration_engine
    execute 'CREATE USER "'||_integration_engine_user||'" WITH PASSWORD '''||_integration_engine_pass||'''';
    execute 'GRANT CONNECT ON DATABASE "'||_dataplatform_database||'" TO "'||_integration_engine_user||'"';
    execute 'SET SEARCH_PATH = "'||_dataplatform_database||'"';
    execute 'GRANT USAGE ON SCHEMA meta TO "'||_integration_engine_user||'"';
    execute 'GRANT SELECT,INSERT,UPDATE,DELETE  ON ALL TABLES IN SCHEMA public, meta TO "'||_integration_engine_user||'"';
    execute 'GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA meta TO "'||_integration_engine_user||'"';
-- create tmp schema for integration engine user
    execute 'CREATE SCHEMA IF NOT EXISTS tmp';
    execute 'GRANT USAGE,CREATE ON SCHEMA tmp TO "'||_integration_engine_user||'"';

-- create and set user for permission proxy
    execute 'CREATE USER "'||_permission_proxy_user||'" WITH PASSWORD '''||_permission_proxy_pass||'''';
    execute 'GRANT CONNECT ON DATABASE "'||_permission_proxy_database||'" TO "'||_permission_proxy_user||'"';
    execute 'SET SEARCH_PATH = "'||_permission_proxy_user||'"';
    execute 'GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO "'||_permission_proxy_user||'"';

-- create function reset_grants

	x_sql := 'CREATE OR REPLACE FUNCTION meta.reset_grants()';
	x_sql := x_sql||chr(10)||'RETURNS void';
	x_sql := x_sql||chr(10)||'LANGUAGE ''plpgsql''';
	x_sql := x_sql||chr(10)||'COST 100';
	x_sql := x_sql||chr(10)||'VOLATILE';
	x_sql := x_sql||chr(10)||'AS $BODY$';
	x_sql := x_sql||chr(10)||'declare';
	x_sql := x_sql||chr(10)||chr(9)||'c integer;';
	x_sql := x_sql||chr(10)||'begin';
    x_sql := x_sql||chr(10)||chr(9)||'GRANT CONNECT ON DATABASE "'||_dataplatform_database||'" TO "'||_output_gateway_user||'";';
    x_sql := x_sql||chr(10)||chr(9)||'GRANT SELECT  ON ALL TABLES IN SCHEMA public, meta TO "'||_output_gateway_user||'";';

-- create and set user for integration_engine
    x_sql := x_sql||chr(10)||chr(9)||'GRANT CONNECT ON DATABASE "'||_dataplatform_database||'" TO "'||_integration_engine_user||'";';
    x_sql := x_sql||chr(10)||chr(9)||'GRANT SELECT,INSERT,UPDATE,DELETE  ON ALL TABLES IN SCHEMA public, meta TO "'||_integration_engine_user||'";';
    x_sql := x_sql||chr(10)||chr(9)||'GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA meta TO "'||_integration_engine_user||'";';
-- create tmp schema for integration engine user
    x_sql := x_sql||chr(10)||chr(9)||'GRANT CREATE,USAGE ON SCHEMA tmp TO "'||_integration_engine_user||'";';

-- create and set user for permission proxy
    x_sql := x_sql||chr(10)||chr(9)||'GRANT CONNECT ON DATABASE "'||_permission_proxy_database||'" TO "'||_permission_proxy_user||'";';
    x_sql := x_sql||chr(10)||chr(9)||'GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO "'||_permission_proxy_user||'";';
	x_sql := x_sql||chr(10)||'end;';		
	x_sql := x_sql||chr(10)||'$BODY$;';		
		--raise notice '%',x_sql;
	execute x_sql;
end$$;
