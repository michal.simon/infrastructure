-- SCHEMA: meta

-- DROP SCHEMA meta ;

CREATE SCHEMA meta
    AUTHORIZATION postgres;

COMMENT ON SCHEMA meta
    IS 'Metadata ';
	
ALTER DEFAULT PRIVILEGES IN SCHEMA meta
GRANT SELECT ON TABLES TO PUBLIC;	
	
-- SCHEMA: history

-- DROP SCHEMA history ;

CREATE SCHEMA history
    AUTHORIZATION postgres;

COMMENT ON SCHEMA history
    IS 'Historizační tabulky plněné trigerem';

ALTER DEFAULT PRIVILEGES IN SCHEMA history
GRANT SELECT ON TABLES TO PUBLIC;	


-- FUNCTION: meta.trg_general()

-- DROP FUNCTION meta.trg_general();

CREATE FUNCTION meta.trg_general()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF 
AS $BODY$

declare 
	sql text;
	operace varchar(1);
  	ri RECORD;
    t TEXT;
	promene text := '';
	hodnoty text := '';
begin

/*
RAISE NOTICE E'\n    Operation: %\n    Schema: %\n    Table: %',
        TG_OP,
        TG_TABLE_SCHEMA,
        TG_TABLE_NAME;
*/		
  FOR ri IN
        SELECT ordinal_position, column_name, data_type
        FROM information_schema.columns
        WHERE
            table_schema = quote_ident(TG_TABLE_SCHEMA)
        AND table_name = quote_ident(TG_TABLE_NAME)
        ORDER BY ordinal_position
    LOOP
		if (TG_OP = 'DELETE') then
        	EXECUTE 'SELECT ($1).' || ri.column_name || '::text' INTO t USING OLD;
		else
			EXECUTE 'SELECT ($1).' || ri.column_name || '::text' INTO t USING NEW;
		end if;
		promene:=promene||ri.column_name||', ';
		if t is null then
			hodnoty:=hodnoty||'null, ';
		else
			hodnoty:=hodnoty||''''||coalesce(t, '')||''', ';
		end if;
		
/*		RAISE NOTICE E'\n    Operation: %\n    Schema: %\n    Table: %\n	column: %\n		value: %\n',
        TG_OP,
        TG_TABLE_SCHEMA,
        TG_TABLE_NAME,
		ri.column_name,
		t;
*/		
    END LOOP;
	
	promene:=promene||'operation,operation_time';

--raise notice E'promene: %', promene;

if (TG_OP = 'DELETE') THEN
	operace='D';
elsif (TG_OP = 'UPDATE') THEN
	operace='U';
elsif (TG_OP = 'INSERT') THEN
	operace='I';
else
 	operace='?';
end if;	

hodnoty:=hodnoty||''''||operace||''','''||cast(now() as varchar(25))||'''';
--raise notice E'hodnoty: %', hodnoty;													
													
sql:= 'INSERT INTO history.'||TG_TABLE_NAME||'('||promene||') values ('||hodnoty||')';
--execute _sql using new;

-- INSERT INTO emp_audit VALUES('D', user, OLD.*);
execute sql;

IF (TG_OP = 'DELETE') THEN
--            INSERT INTO "PR_static_h" SELECT  OLD.*,'D', now(), user;
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
--            INSERT INTO "PR_static_h" SELECT NEW.*,'U', now(), user ;
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
--            INSERT INTO "PR_static_h" SELECT NEW.*,'I', now(), user ;
            RETURN NEW;
        END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
end;		

$BODY$;

ALTER FUNCTION meta.trg_general()
    OWNER TO postgres;

	
	


-- FUNCTION: meta.add_audit_field(character varying, character varying)

-- DROP FUNCTION meta.add_audit_field(character varying, character varying);

CREATE OR REPLACE FUNCTION meta.add_audit_field(
	p_table_schema character varying,
	p_table_name character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare
	sql varchar(5000);
begin
-- kontrola existence tabulky
	if not exists (	select table_name from information_schema.tables where upper(table_name) = upper(p_table_name) and upper(p_table_schema) = upper(table_schema))	then
		raise EXCEPTION 'Tabulka (%) neexistuje', p_table_name;
	end if;

-- /kontrola
-- create
	-- CREATE_BATCH_ID
	if not exists 
	(	
		select column_name from information_schema.columns 
		where upper(table_name) = upper(p_table_name) 
		and upper(p_table_schema) = upper(table_schema)
		and upper(column_name) = 'CREATE_BATCH_ID'
	)	then
	-- sloupec chybí - přidáme
		sql := 'alter table ' || p_table_schema||'.'||p_table_name || ' add CREATE_BATCH_ID bigint';
		EXECUTE sql;
		
		sql := 'COMMENT ON COLUMN ' ||  p_table_schema||'.'||p_table_name || '.CREATE_BATCH_ID is ''ID vstupní dávky''' ;
		EXECUTE sql;
	end if;
	-- /CREATE_BATCH_ID
	-- CREATED_at
	if not exists 
	(	
		select column_name from information_schema.columns where upper(table_name) = upper(p_table_name) and upper(p_table_schema) = upper(table_schema) and upper(column_name) = 'CREATED_AT'
	)	then

	-- sloupec chybí - přidáme
		sql := 'alter table ' || p_table_schema||'.'||p_table_name || ' add CREATED_AT TIMESTAMP';
		EXECUTE sql;
		
		sql := 'COMMENT ON COLUMN ' ||  p_table_schema||'.'||p_table_name || '.CREATED_AT is ''Čas vložení''' ;
		EXECUTE sql;

	end if;
	-- /CREATED_AT
	-- CREATED_BY
	if not exists 
	(	
		select column_name from information_schema.columns where upper(table_name) = upper(p_table_name) and upper(p_table_schema) = upper(table_schema) and upper(column_name) = 'CREATED_BY'
	)	then

	-- sloupec chybí - přidáme
		sql := 'alter table ' ||  p_table_schema||'.'||p_table_name || ' add CREATED_BY varchar(150)';
		EXECUTE sql;
		
		sql := 'COMMENT ON COLUMN ' ||  p_table_schema||'.'||p_table_name || '.CREATED_BY is ''identikace uživatele/procesu, který záznam vložil''' ;
		EXECUTE sql;

	end if;
	-- /CREATED_BY
-- /create
-- update
	-- UPDATE_BATCH_ID
	if not exists 
	(	
		select column_name from information_schema.columns where upper(table_name) = upper(p_table_name) and upper(p_table_schema) = upper(table_schema) and upper(column_name) = 'UPDATE_BATCH_ID'
	)	then

	-- sloupec chybí - přidáme
		sql := 'alter table ' ||  p_table_schema||'.'||p_table_name || ' add UPDATE_BATCH_ID bigint';
		EXECUTE sql;
		
		sql := 'COMMENT ON COLUMN ' ||  p_table_schema||'.'||p_table_name || '.UPDATE_BATCH_ID is ''ID poslední dávky, která záznam modifikovala''' ;
		EXECUTE sql;

	end if;
	-- /UPDATE_BATCH_ID
	-- UPDATED_AT
	if not exists 
	(	
		select column_name from information_schema.columns where upper(table_name) = upper(p_table_name) and upper(p_table_schema) = upper(table_schema) and upper(column_name) = 'UPDATED_AT'
	)	then

	-- sloupec chybí - přidáme
		sql := 'alter table ' ||  p_table_schema||'.'||p_table_name || ' add UPDATED_AT TIMESTAMP';
		EXECUTE sql;
		
		sql := 'COMMENT ON COLUMN ' ||  p_table_schema||'.'||p_table_name || '.UPDATED_AT is ''Čas poslední modifikace''' ;
		EXECUTE sql;

	end if;
	-- /UPDATED_AT
	-- UPDATED_BY
	if not exists 
	(	
		select column_name from information_schema.columns where upper(table_name) = upper(p_table_name) and upper(p_table_schema) = upper(table_schema) and upper(column_name) = 'UPDATED_BY'
	)	then

	-- sloupec chybí - přidáme
		sql := 'alter table ' ||  p_table_schema||'.'||p_table_name || ' add UPDATED_BY varchar(150)';
		EXECUTE sql;
		
		sql := 'COMMENT ON COLUMN ' ||  p_table_schema||'.'||p_table_name || '.UPDATED_BY is ''Identikace uživatele/procesu, který poslední měnil záznam''' ;
		EXECUTE sql;

	end if;
	-- /UPDATED_BY
-- /update

-- indexy
	if not exists 
	(	
		select indexname from pg_indexes
		where indexname  =  p_table_name || '_create_batch' 
		and upper(p_table_schema) = upper(schemaname)
	)	then

		sql := 'create index ' ||  p_table_name || '_create_batch on '|| p_table_schema||'.'||p_table_name|| '(CREATE_BATCH_ID)';
		EXECUTE sql;
	end if;

	if not exists 
	(	
		select indexname from pg_indexes
		where indexname  =  p_table_name || '_update_batch'
		and upper(p_table_schema) = upper(schemaname)
	)	then

		sql := 'create index ' ||  p_table_name || '_update_batch on '|| p_table_schema||'.'||p_table_name|| '(UPDATE_BATCH_ID)';
		EXECUTE sql;
	end if;

-- /indexy

	return 0;
end;

$BODY$;

ALTER FUNCTION meta.add_audit_field(character varying, character varying)
    OWNER TO postgres;

COMMENT ON FUNCTION meta.add_audit_field(character varying, character varying)
    IS 'Přidá do existující tabulky auditní sloupce';



-- FUNCTION: meta.hist(character varying)

-- DROP FUNCTION meta.hist(character varying);

CREATE OR REPLACE FUNCTION meta.hist(
	p_table_name character varying)
    RETURNS void
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare
	pocet integer;
	sql text;
	query_rec record;
	trg_name varchar(150);
begin
--	raise notice 'Zaciname ---';

-- overerni existence tabulky
	if not exists (select table_name from information_schema.tables where upper(table_name) = upper(p_table_name) and table_schema = 'public') then
		raise EXCEPTION 'Tabulka (%) neexistuje', p_table_name;
	end if;
	
-- overeni existence hist tabulky
	if not exists (select table_name from information_schema.tables	where upper(table_name) = upper(p_table_name)	and table_schema = 'history') then
	-- vytvoreni historicke tabulky (nexistuje)
		sql := 'create table history.'||p_table_name||' (';

		FOR  query_rec IN 
			select column_name, data_type, character_maximum_length,numeric_precision,numeric_scale
			from  information_schema.columns
			where table_schema = 'public'
			and table_name = p_table_name
			order by ordinal_position
		LOOP
			sql:=sql||query_rec.column_name||' '||query_rec.data_type;
			if query_rec.character_maximum_length is not null then
				sql:=sql||'('||cast(query_rec.character_maximum_length as varchar(50))||'),';
			elsif COALESCE(query_rec.numeric_scale,0) >0 then
				sql:=sql||'('||cast(query_rec.numeric_precision as varchar(50))||','||cast(query_rec.numeric_scale as varchar(50))||'),';
			else																
				sql:=sql||',';
			end if;
		END LOOP;
		sql:=sql||'operation char(1), operation_time timestamp without time zone) TABLESPACE pg_default';
--		raise notice 'Value: %', sql;
		execute sql;
	else							
		raise notice 'Historicka tabulka % jiz existuje', p_table_name;
	end if;
--	Vytvoření triggeru

	trg_name := 'trg_'||p_table_name;
	
	if not exists (select tgname from pg_trigger where tgname = trg_name) then
		sql:='CREATE TRIGGER trg_'||p_table_name||' BEFORE INSERT OR DELETE OR UPDATE ON public.'||p_table_name||' FOR EACH ROW EXECUTE PROCEDURE meta.trg_general()';
		execute sql;
	else																
		raise notice 'Trigger % jiz existuje', trg_name;
	end if;
																															  
end;

$BODY$;

ALTER FUNCTION meta.hist(character varying)
    OWNER TO postgres;

COMMENT ON FUNCTION meta.hist(character varying)
    IS 'Vytvoření historické tabulky';


-- FUNCTION: meta.hist(character varying, character varying)

-- DROP FUNCTION meta.hist(character varying, character varying);

CREATE OR REPLACE FUNCTION meta.hist(
	p_schema character varying,
	p_table_name character varying)
    RETURNS void
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

-- přetížení původní fce, přidáno schéma
declare
	pocet integer;
	sql text;
	query_rec record;
	trg_name varchar(150);
begin
--	raise notice 'Zaciname ---';

-- overerni existence tabulky
	if not exists (select table_name from information_schema.tables where upper(table_name) = upper(p_table_name) and upper(table_schema) = upper(p_schema))
	then
		raise EXCEPTION 'Tabulka (%) neexistuje', p_table_name;
	end if;
	
-- overeni existence hist tabulky
	if not exists (select table_name from information_schema.tables	where upper(table_name) = upper(p_table_name)	and table_schema = 'history')
	then
	-- vytvoreni historicke tabulky (nexistuje)
		sql := 'create table history.'||p_table_name||' (';

		FOR  query_rec IN 
			select column_name, data_type, character_maximum_length,numeric_precision,numeric_scale
			from  information_schema.columns
			where table_schema = p_schema
			and table_name = p_table_name
			order by ordinal_position
		LOOP
			sql:=sql||query_rec.column_name||' '||query_rec.data_type;
			if query_rec.character_maximum_length is not null then
				sql:=sql||'('||cast(query_rec.character_maximum_length as varchar(50))||'),';
			elsif COALESCE(query_rec.numeric_scale,0) >0 then
				sql:=sql||'('||cast(query_rec.numeric_precision as varchar(50))||','||cast(query_rec.numeric_scale as varchar(50))||'),';
			else																
				sql:=sql||',';
			end if;
		END LOOP;
		sql:=sql||'operation char(1), operation_time timestamp without time zone) TABLESPACE pg_default';
--		raise notice 'Value: %', sql;
		execute sql;
	else							
		raise notice 'Historicka tabulka % jiz existuje', p_table_name;
	end if;
--	Vytvoření triggeru

	trg_name := 'trg_'||p_table_name;
	
	if not exists(select tgname from pg_trigger where tgname = trg_name) then
		sql:='CREATE TRIGGER trg_'||p_table_name||' BEFORE INSERT OR DELETE OR UPDATE ON '||p_schema||'.'||p_table_name||' FOR EACH ROW EXECUTE PROCEDURE meta.trg_general()';
		execute sql;
	else																
		raise notice 'Trigger % jiz existuje', trg_name;
	end if;
																															  
end;

$BODY$;

ALTER FUNCTION meta.hist(character varying, character varying)
    OWNER TO postgres;

COMMENT ON FUNCTION meta.hist(character varying, character varying)
    IS 'Vytvoření historické tabulky';

	

	
	
	
	
-- SEQUENCE: meta.batch_id_batch_seq

-- DROP SEQUENCE meta.batch_id_batch_seq;

CREATE SEQUENCE meta.batch_id_batch_seq;

ALTER SEQUENCE meta.batch_id_batch_seq
    OWNER TO postgres;


		
-- SEQUENCE: meta.db_type_id_db_seq

-- DROP SEQUENCE meta.db_type_id_db_seq;

CREATE SEQUENCE meta.db_type_id_db_seq;

ALTER SEQUENCE meta.db_type_id_db_seq
    OWNER TO postgres;



-- Table: meta.db_type

-- DROP TABLE meta.db_type;

CREATE TABLE meta.db_type
(
    id_db integer NOT NULL DEFAULT nextval('meta.db_type_id_db_seq'::regclass),
    name_db character varying(150) COLLATE pg_catalog."default",
    con_string_db character varying(500) COLLATE pg_catalog."default",
    "user" character varying(50) COLLATE pg_catalog."default",
    password character varying(50) COLLATE pg_catalog."default",
    created_at timestamp without time zone DEFAULT now(),
    CONSTRAINT db_type_pkey PRIMARY KEY (id_db),
    CONSTRAINT db_type_name UNIQUE (name_db)

)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE meta.db_type
    OWNER to postgres;
COMMENT ON TABLE meta.db_type
    IS 'seznam použitých databází';

COMMENT ON COLUMN meta.db_type.id_db
    IS 'id databáze';

COMMENT ON COLUMN meta.db_type.name_db
    IS 'Název db (unikátní)';

COMMENT ON COLUMN meta.db_type.con_string_db
    IS 'Connection string pro přístup do db';

COMMENT ON COLUMN meta.db_type."user"
    IS 'user name';

COMMENT ON CONSTRAINT db_type_name ON meta.db_type
    IS 'unikátní název';
	
insert into meta.db_type (id_db,name_db) values (1,'postgres');	
insert into meta.db_type (id_db,name_db) values (2,'mongodb');	
	
	
-- SEQUENCE: meta.dataset_id_dataset_seq

-- DROP SEQUENCE meta.dataset_id_dataset_seq;

CREATE SEQUENCE meta.dataset_id_dataset_seq;

ALTER SEQUENCE meta.dataset_id_dataset_seq
    OWNER TO postgres;	



-- Table: meta.dataset

-- DROP TABLE meta.dataset;

CREATE TABLE meta.dataset
(
    id_dataset integer NOT NULL DEFAULT nextval('meta.dataset_id_dataset_seq'::regclass),
    code_dataset character varying(50) COLLATE pg_catalog."default" NOT NULL,
    name_dataset character varying(250) COLLATE pg_catalog."default" NOT NULL,
    descr text COLLATE pg_catalog."default",
    ret_days integer,
    h_ret_days integer,
    created_at timestamp without time zone DEFAULT now(),
    CONSTRAINT dataset_pkey PRIMARY KEY (id_dataset),
    CONSTRAINT dataset_code UNIQUE (code_dataset)
,
    CONSTRAINT dataset_name UNIQUE (name_dataset)

)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE meta.dataset
    OWNER to postgres;
COMMENT ON TABLE meta.dataset
    IS 'Datová sada';

COMMENT ON COLUMN meta.dataset.id_dataset
    IS 'id datové sady';

COMMENT ON COLUMN meta.dataset.code_dataset
    IS 'Zkrácený název datové sady, tento kód bude použit při tvorbě názvu tabulky jako prefix (unikátní)';

COMMENT ON COLUMN meta.dataset.name_dataset
    IS 'Název datové sady (unikátní)';

COMMENT ON COLUMN meta.dataset.descr
    IS 'Popis datové sady';

COMMENT ON COLUMN meta.dataset.ret_days
    IS 'Retence datové sady ve dnech. NULL nebo 0 znamená bez retence';

COMMENT ON COLUMN meta.dataset.h_ret_days
    IS 'Retence datové sady(historické tabulky) ve dnech. NULL nebo 0 znamená bez retence';

COMMENT ON CONSTRAINT dataset_code ON meta.dataset
    IS 'Unikátní code';
COMMENT ON CONSTRAINT dataset_name ON meta.dataset
    IS 'Unikátní jméno datové sady';

	
-- HISTORICKÁ TABULKA
SELECT meta.hist('meta','dataset');
	
-- Trigger: trg_dataset

DROP TRIGGER trg_dataset ON meta.dataset;

CREATE TRIGGER trg_dataset
    BEFORE INSERT OR DELETE OR UPDATE 
    ON meta.dataset
    FOR EACH ROW
    EXECUTE PROCEDURE meta.trg_general();

-- INI LOAD	
insert into meta.dataset
values (1,'META','Tabluky metadat','řídící struktury pro databázi',null,null,cast(now() as timestamp));

		
	

-- Table: meta.batch

-- DROP TABLE meta.batch;

CREATE TABLE meta.batch
(
    id_batch bigint NOT NULL DEFAULT nextval('meta.batch_id_batch_seq'::regclass),
    id_dataset integer NOT NULL,
    statut_batch character varying(20) COLLATE pg_catalog."default" NOT NULL DEFAULT 'New'::character varying,
    start_batch timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    end_batch timestamp without time zone,
    created_at timestamp without time zone DEFAULT now(),
    CONSTRAINT batch_pkey PRIMARY KEY (id_batch),
    CONSTRAINT batch_dataset FOREIGN KEY (id_dataset)
        REFERENCES meta.dataset (id_dataset) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE meta.batch
    OWNER to postgres;
COMMENT ON TABLE meta.batch
    IS 'Datová dávka';

COMMENT ON COLUMN meta.batch.id_dataset
    IS 'id datové sady';

COMMENT ON COLUMN meta.batch.statut_batch
    IS 'Stav dávky';

COMMENT ON COLUMN meta.batch.end_batch
    IS 'Ukončení zpracování';
	
	
-- ini load
INSERT INTO meta.batch(
	id_batch, id_dataset, statut_batch, start_batch, end_batch, created_at)
	VALUES 
		(-2, 1, 'manual', cast(now() as timestamp), null, cast(now() as timestamp)),
		(-1, 1, 'iniLoad', cast(now() as timestamp), null, cast(now() as timestamp));
		

	
	
	
-- Table: meta."extract"

-- DROP TABLE meta."extract";

CREATE TABLE meta."extract"
(
    name_extract character varying(150) COLLATE pg_catalog."default" NOT NULL,
    id_dataset integer NOT NULL,
    id_db integer NOT NULL,
    descr text COLLATE pg_catalog."default",
    schema_extract character varying(150) COLLATE pg_catalog."default" DEFAULT 'public'::character varying,
    ret_days integer,
    h_ret_days integer,
    created_at timestamp without time zone DEFAULT now(),
    CONSTRAINT extract_pkey PRIMARY KEY (name_extract),
    CONSTRAINT extract_dataset FOREIGN KEY (id_dataset)
        REFERENCES meta.dataset (id_dataset) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT extract_db FOREIGN KEY (id_db)
        REFERENCES meta.db_type (id_db) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE meta."extract"
    OWNER to postgres;
COMMENT ON TABLE meta."extract"
    IS 'Tabulky (kolekece) datové sady';

COMMENT ON COLUMN meta."extract".name_extract
    IS 'Názeva tabulky/kolekce';

COMMENT ON COLUMN meta."extract".id_dataset
    IS 'FK do db_dataset';

COMMENT ON COLUMN meta."extract".id_db
    IS 'typ databáze';

COMMENT ON COLUMN meta."extract".ret_days
    IS 'Retence extraktu, pokud je odlišná od retence datové sady ve dnech. NULL znamená, že bude použita retence sady, 0 znamená, že extrakt bude bez retence bez ohledu na na stavení retence sady';

COMMENT ON COLUMN meta."extract".h_ret_days
    IS 'Retence extraktu (historická tabulka), pokud je odlišná od historické retence datové sady ve dnech. NULL znamená, že bude použita retence sady, 0 znamená, že extrakt bude bez retence bez ohledu na na stavení retence sady';

COMMENT ON CONSTRAINT extract_dataset ON meta."extract"
    IS 'Vazba na dataset';
COMMENT ON CONSTRAINT extract_db ON meta."extract"
    IS 'Vazby na typ databáze';

	
-- HISTORICKÁ TABULKA
SELECT meta.hist('meta','extract');
	
	
-- Trigger: trg_extract

DROP TRIGGER trg_extract ON meta."extract";

CREATE TRIGGER trg_extract
    BEFORE INSERT OR DELETE OR UPDATE 
    ON meta."extract"
    FOR EACH ROW
    EXECUTE PROCEDURE meta.trg_general();
	
-- ini load
INSERT INTO meta."extract"(
	name_extract, id_dataset, id_db, descr, schema_extract, ret_days, h_ret_days, created_at)
	VALUES 
	('batch', 1, 1, 'Popis jednotlivých importních dávaek', 'meta', null, null, cast(now() as timestamp)),
	('dataset', 1, 1, 'Datové sady', 'meta', null, null, cast(now() as timestamp)),
	('db_type', 1, 1, 'databáze použité mna DP (číselník)', 'meta', null, null, cast(now() as timestamp)),
	('extract', 1, 1, 'Tabulky/kolekce jednotlivých datových sad', 'meta', null, null, cast(now() as timestamp)),
	('summary_extract', 1, 1, 'Počty záznamů a stav jednotlivé importní dávky', 'meta', null, null, cast(now() as timestamp)),
	('batch_validation', 1, 1, 'Validační hodnoty pro jednotlivé dávka', 'meta', null, null, cast(now() as timestamp));
	



-- Table: meta.summary_extract

-- DROP TABLE meta.summary_extract;

CREATE TABLE meta.summary_extract
(
    id_batch bigint NOT NULL,
    name_extract character varying(50) COLLATE pg_catalog."default" NOT NULL,
    statut character varying(20) COLLATE pg_catalog."default" NOT NULL,
    insert_count integer,
    update_count integer,
    delete_count integer,
    created_at timestamp without time zone DEFAULT now(),
    CONSTRAINT sumext_pkey PRIMARY KEY (id_batch, name_extract),
    CONSTRAINT sumext_batch FOREIGN KEY (id_batch)
        REFERENCES meta.batch (id_batch) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT sumext_extname FOREIGN KEY (name_extract)
        REFERENCES meta."extract" (name_extract) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE meta.summary_extract
    OWNER to postgres;
COMMENT ON TABLE meta.summary_extract
    IS 'Počty záznamů v dávce a extractu';

COMMENT ON COLUMN meta.summary_extract.id_batch
    IS 'id dávky';

COMMENT ON COLUMN meta.summary_extract.name_extract
    IS 'Název extractu (FK)';	



-- SEQUENCE: meta.batch_validation_id_seq

-- DROP SEQUENCE meta.batch_validation_id_seq;

CREATE SEQUENCE meta.batch_validation_id_seq;

ALTER SEQUENCE meta.batch_validation_id_seq
    OWNER TO postgres;


-- Table: meta.batch_validation

-- DROP TABLE meta.batch_validation;

CREATE TABLE meta.batch_validation
(
    id bigint NOT NULL DEFAULT nextval('meta.batch_validation_id_seq'::regclass),
    dataset bigint NOT NULL,
    id_batch bigint NOT NULL,
    key character varying(255) COLLATE pg_catalog."default",
    type character varying(255) COLLATE pg_catalog."default",
    value character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT batch_validation_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE meta.batch_validation
    OWNER to postgres;	
	
	
-- functions

-- FUNCTION: meta.create_batch(integer)

-- DROP FUNCTION meta.create_batch(integer);

CREATE OR REPLACE FUNCTION meta.create_batch(
	p_id_dataset integer)
    RETURNS bigint
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare
	new_batch bigint;

begin
	-- zjištění zda existuje vstupní sada
	if not exists (select id_dataset from meta.dataset where id_dataset = p_id_dataset)	then
		raise EXCEPTION 'Datová sada (%) neexistuje', p_id_dataset;
	end if;
	
	insert into meta.batch (id_dataset,statut_batch,start_batch)
	values (p_id_dataset,'NEW',current_Timestamp)
	returning id_batch into new_batch;
	
	return new_batch;
end;

$BODY$;

ALTER FUNCTION meta.create_batch(integer)
    OWNER TO postgres;

COMMENT ON FUNCTION meta.create_batch(integer)
    IS 'Fce zakládající záznam o nové dávce. Vrací číslo dávky';

	
	
-- FUNCTION: meta.close_extract(integer, character varying, integer, integer, integer)

-- DROP FUNCTION meta.close_extract(integer, character varying, integer, integer, integer);

CREATE OR REPLACE FUNCTION meta.close_extract(
	p_id_batch integer,
	p_extract character varying,
	p_insert integer,
	p_update integer,
	p_delete integer)
    RETURNS void
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare
begin
	-- zjištění existence dávky a její statut
	if not exists (SELECT id_batch FROM meta.batch WHERE id_batch = p_id_batch and statut_batch = 'New')
	then
		raise EXCEPTION 'Dávka (%) neexistuje, nebo nemí ve stavu NEW', p_id_batch;
	end if;
	
	insert into meta.summary_extract (id_batch, name_extract, statut, insert_count, update_count,delete_count)
		VALUES (p_id_batch, xtable, 'OK', p_insert, p_update, p_delete)
	on conflict (id_batch, name_extract)
	do 
		update set statut='OK', insert_count = p_insert, update_count=P_update,delete_count=p_delete;
end;

$BODY$;

ALTER FUNCTION meta.close_extract(integer, character varying, integer, integer, integer)
    OWNER TO postgres;

COMMENT ON FUNCTION meta.close_extract(integer, character varying, integer, integer, integer)
    IS 'Fce uzavírá existující extrakt v rámci dávky dávky (voláno z MongoDb)';


-- FUNCTION: meta.count_batch(character varying, character varying, bigint)

-- DROP FUNCTION meta.count_batch(character varying, character varying, bigint);

CREATE OR REPLACE FUNCTION meta.count_batch(
	p_table_name character varying,
	p_schema character varying,
	p_id_batch bigint,
	OUT p_count_insert integer,
	OUT p_count_upd integer)
    RETURNS record
    LANGUAGE 'plpgsql'

    COST 100
    STABLE 
AS $BODY$

declare
	sql varchar(50000);
begin
	if p_schema = '' then
		p_schema = 'public';
	end if;
	-- overeni batche
	if not exists (SELECT id_batch FROM meta.batch WHERE id_batch = p_id_batch)
	then
		raise EXCEPTION 'Dávka (%) neexistuje.', p_id_batch;
	end if;
	
	-- overeni tabulky
	if not exists (select table_name FROM information_schema.tables where table_name = p_table_name	and table_schema = p_schema)
	then
		raise EXCEPTION 'Tabulka (%) neexistuje.', p_table_name;
	end if;
	
	sql := 'select count(*) from '||p_table_name||' where create_batch_id = '||p_id_batch;
	begin
		execute sql WHENEVER into p_count_insert ;
	exception when others then
		p_count_insert :=0;
	end;

	sql := 'select count(*) from '||p_table_name||' where update_batch_id = '||p_id_batch;
	begin
		execute sql WHENEVER into p_count_upd ;
	exception when others then
		p_count_upd :=0;
	end;

end;

$BODY$;

ALTER FUNCTION meta.count_batch(character varying, character varying, bigint)
    OWNER TO postgres;

COMMENT ON FUNCTION meta.count_batch(character varying, character varying, bigint)
    IS 'Zjišťuje počty modifikovaných záznamů v tabulce';

	
	

-- FUNCTION: meta.close_batch(integer, character varying)

-- DROP FUNCTION meta.close_batch(integer, character varying);

CREATE OR REPLACE FUNCTION meta.close_batch(
	p_id_batch integer,
	p_statut character varying)
    RETURNS void
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare
	pocet integer;
	xid_dataset int;
	xschema varchar(50);
	xtable varchar(150);
	query_rec record;
	query_rec1 record;
	xinsert integer;
	xupd integer;
	xstatut varchar(50);

begin
	-- zjištění vstupní sada
	select id_dataset,statut_batch into xid_dataset,xstatut
	from meta.batch
	where id_batch = p_id_batch;
	
	if xid_dataset is null then
		raise EXCEPTION 'Dávka (%) neexistuje, nebo nemá nastaven správný dataset', p_id_batch;
	end if;
	
	if not xstatut  =  'NEW' then
		raise EXCEPTION 'Dávka (%) není ve stavu NEW', p_id_batch;
	end if;
	
	if upper(p_statut) = 'ERROR' then
		update meta.batch
		set statut_batch = p_statut,
		end_batch = current_Timestamp
		where id_batch = p_id_batch;
		
		return;
	end if;

	FOR  query_rec IN 
		select schema_extract,name_extract  
		from meta.extract 
		where id_dataset = xid_dataset and id_db = 1
	LOOP
		if not exists (SELECT * FROM meta.summary_extract WHERE name_extract=query_rec.name_extract and statut = 'OK' and id_batch = p_id_batch)
		then
			xschema := query_rec.schema_extract;
			xtable := query_rec.name_extract;
			select p_count_insert,p_count_upd into xinsert,xupd
			from meta.count_batch(xtable,xschema,p_id_batch);

			if xinsert+xupd >0 then

				insert into meta.summary_extract (id_batch, name_extract, statut, insert_count, update_count)
					VALUES (p_id_batch, xtable, p_statut, xinsert, xupd)
				on conflict (id_batch, name_extract)
				do 
					update set statut=p_statut, insert_count = xinsert, update_count=xupd;
			end if;
		end if;
	END LOOP;
	
	update meta.batch
	set statut_batch = p_statut,
	end_batch = current_Timestamp
	where id_batch = p_id_batch;
	
end;

$BODY$;

ALTER FUNCTION meta.close_batch(integer, character varying)
    OWNER TO postgres;

COMMENT ON FUNCTION meta.close_batch(integer, character varying)
    IS 'Fce uzavírá existující dávku';
	
	

	
	
-- FUNCTION: meta.retention()

-- DROP FUNCTION meta.retention();

CREATE OR REPLACE FUNCTION meta.retention(
	)
    RETURNS void
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare 
	query_dataset record;
	query_extract record;
	x_retence integer;		-- retence tabulky
	x_h_retence integer;	-- retence historické tabulky
	cur_time timestamp;
	cur_time_h timestamp;
	txt_sql text;
	c integer;
begin

-- cyklus pře všechny sady
	FOR  query_dataset IN 
		select id_dataset,ret_days,h_ret_days,code_dataset
		from meta.dataset
	LOOP
	RAISE notice E'sada:% (id:%)',query_dataset.code_dataset,query_dataset.id_dataset;
	-- cyklus pře všechny extrakty
		FOR  query_extract IN 
			select name_extract,schema_extract,ret_days,h_ret_days
			from meta.extract
			where id_dataset = query_dataset.id_dataset
		LOOP
		RAISE notice E'extract:%',query_extract.name_extract;
		
		-- kontrola existence tabulky
		if not exists (SELECT tablename FROM pg_tables WHERE tablename=query_extract.Name_extract and schemaname = coalesce(query_extract.schema_extract,'public'))
		then
			RAISE notice 'Table %.% does not exist.', query_extract.schema_extract,query_extract.Name_extract;
		else
			if
			-- kontrola existence sloupce created_at
			not  exists
				(SELECT table_name FROM information_schema.columns
					WHERE 
					table_name   = query_extract.name_extract
				 	and table_schema = query_extract.schema_extract
					and column_name = 'created_at'
				)
			then
				RAISE notice 'Column created_at does not exist in %.%.', query_extract.schema_extract,query_extract.name_extract;
			else
-- nastaveni retence x_retence
				if query_extract.ret_days is not null then 
					x_retence := query_extract.ret_days;
				else
					x_retence := query_dataset.ret_days;
				end if;
				if 	coalesce(x_retence,0) > 0 then																							
					cur_time = now() - x_retence*interval '1 day';
					--txt_sql = 'DELETE FROM %1.%2 where created_at < cur_time';

--					txt_sql = 'select count(*) FROM ';
					txt_sql = 'delete FROM ';
					txt_sql:=txt_sql||coalesce(query_extract.schema_extract,'public');
					txt_sql:=txt_sql||'.'||query_extract.name_extract||' where created_at < '''||cast(cur_time as varchar(50))||'''';
					execute txt_sql; --into c 

						--coalesce(query_extract.schema_extract,'public'),
						--query_extract.name_extract;
					GET DIAGNOSTICS c := ROW_COUNT;
					--select result.rowCount into c;
					--return c;
					RAISE notice E'Počet smazaných záznamů %', c;
				else 											
					RAISE notice E'retence není nastavena pro tento extrakt';
				end if;
			end if;
		end if;
																															
																															
-- historické tabulky
-- kontrola existence tabulky
		if not exists (SELECT tablename FROM pg_tables WHERE tablename=query_extract.Name_extract and schemaname = 'history')
		then
			RAISE notice 'Table history.% does not exist.', query_extract.Name_extract;
		else
			if
			-- kontrola existence sloupce created_at
			not  exists
				(SELECT table_name FROM information_schema.columns
					WHERE 
					table_name   = query_extract.name_extract
				 	and table_schema = 'history'
					and column_name = 'operation_time'
				)
			then
				RAISE notice 'Column operation_time does not exist in history.%.', query_extract.name_extract;
			else
-- nastaveni retence x_h_retence
				if query_extract.ret_days is not null then 
					x_h_retence := query_extract.h_ret_days;
				else
					x_h_retence := query_dataset.h_ret_days;
				end if;
				if 	coalesce(x_h_retence,0) > 0 then																							
					cur_time_h = now() - x_h_retence*interval '1 day';
					--txt_sql = 'DELETE FROM %1.%2 where created_at < cur_time';

--					txt_sql = 'select count(*) FROM ';
					txt_sql = 'delete FROM history';
					txt_sql:=txt_sql||'.'||query_extract.name_extract||' where operation_time < '''||cast(cur_time_h as varchar(50))||'''';
					execute txt_sql; --into c 

						--coalesce(query_extract.schema_extract,'public'),
						--query_extract.name_extract;
					GET DIAGNOSTICS c := ROW_COUNT;
					--select result.rowCount into c;
					--return c;
					RAISE notice E'Počet smazaných záznamů hist.%', c;
				else 											
					RAISE notice E'retence není nastavena pro historii tohoto extraktu.';
				end if;
			end if;
		end if;
																															

		END LOOP; 
	-- /cyklus pře všechny extrakty
	END LOOP; 
-- \cyklus pře všechny sady
end;

$BODY$;

ALTER FUNCTION meta.retention()
    OWNER TO postgres;
	
	
-- Obecná fce na import json

-- FUNCTION: meta.import_general(bigint, json, character varying, character varying, json, json, character varying)

-- DROP FUNCTION meta.import_general(bigint, json, character varying, character varying, json, json, character varying);

CREATE OR REPLACE FUNCTION meta.import_general(
	p_batch_id bigint,
	p_data json,
	p_table_schema character varying,
	p_table_name character varying,
	p_pk json,
	p_sort json,
	p_worker_name character varying,
	OUT x_inserted json,
	OUT x_updated json)
    RETURNS record
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

	
declare 
	pole_seznam varchar;
	pole_sort varchar;
	pole_seznam_datatype varchar;
	pole_pk varchar;
	pole_pk_tar varchar;
	query_rec record;
	x_sql varchar;
	x_sql_sor varchar;
	x_pole varchar;
	x_data_typ varchar;
	x_lenght integer;
	x_sql_where varchar;
	prvni_pole varchar;
--	
	p_cas timestamp;
begin
-- vzor P_pk: [{"pole": "pole1"},{"pole": "sloupec2"},{"pole": "dasi-sloupec"}]

-- kontrola pokud je vyplněn p_sort, musí být i p_pk
if p_sort is not null and p_pk is null then
	raise EXCEPTION 'Nelze vyplnit p_sort(%) a nemí vyplněn PK', p_sort;
end if;

-- kontrola existence tabulky
	if not exists 
	(
		select table_name 
		from information_schema.tables 
		where upper(table_name) = upper(p_table_name) 
		and upper(table_schema) = upper(p_table_schema)
	)	then
		raise EXCEPTION 'Tabulka (%.%) neexistuje', p_table_schema,p_table_name;
	end if;
-- /kontrola existence tabulky	
drop table if exists pp_data;

create temp table pp_data as 
select p_data  as data;

--execute 'select p_data as data';
-- pole z json
FOR  query_rec IN 
	select json_object_keys(data->0) pole
	from pp_data
LOOP
	if not exists 
	(
		select column_name
		from information_schema.columns
		where upper(table_schema) = upper(p_table_schema)
		and upper(table_name) = upper(p_table_name) 
		and upper (column_name) = upper(query_rec.pole)
	)	then
		raise EXCEPTION 'Tabulka %.% neobsahuje pole %', p_table_schema,p_table_name,query_rec.pole;
	else
		if pole_seznam is null then
			pole_seznam := query_rec.pole;
		else
			pole_seznam := pole_seznam ||', '||query_rec.pole;
		end if;
	end if;
end loop;
-- /pole z json

-- Kontrola PK
FOR  query_rec IN 
	select cast(json_array_elements(data)->>'pole' as varchar) pole
	from (select p_pk as data) a 
LOOP
	if not exists 
	(
		select column_name
		from information_schema.columns
		where upper(table_schema) = upper(p_table_schema)
		and upper(table_name) = upper(p_table_name) 
		and upper (column_name) = upper(query_rec.pole)
	)	then
		raise EXCEPTION 'Tabulka %.% neobsahuje PK pole %', p_table_schema,p_table_name,query_rec.pole;
	else
		if pole_pk is null then
			pole_pk := query_rec.pole;
			pole_pk_tar := 'tar.'||query_rec.pole;
			x_sql_where := 'tar.'||query_rec.pole||' = sor.'||query_rec.pole;
		else
			pole_pk := pole_pk ||', '||query_rec.pole;
			pole_pk_tar := pole_pk_tar ||', tar.'||query_rec.pole;
			x_sql_where := x_sql_where||' and tar.'||query_rec.pole||' = sor.'||query_rec.pole;
		end if;
	end if;
end loop;
-- /Kontrola PK

-- kontrola sort
FOR  query_rec IN 
	select cast(json_array_elements(data)->>'pole' as varchar) pole
	from (select p_sort as data) a 
LOOP
	if not exists 
	(
		select column_name
		from information_schema.columns
		where upper(table_schema) = upper(p_table_schema)
		and upper(table_name) = upper(p_table_name) 
		and upper (column_name) = upper(query_rec.pole)
	)	then
		raise EXCEPTION 'Tabulka %.% neobsahuje sort pole %', p_table_schema,p_table_name,query_rec.pole;
	else
		if pole_sort is null then
			pole_sort := query_rec.pole;
		else
			pole_sort := pole_sort ||', '||query_rec.pole;
		end if;
	end if;
end loop;
-- /kontrol sort

/*
fce provádí merge p_data do tabulky vehicleposition_trips
výstup do dvou polí (záznamy insertované a updatované)
*/

--raise notice 'kontroly OK';

drop table if exists mytemp;
--create temp table mytemp(id varchar);
x_sql := 'create temp table mytemp (';
FOR  query_rec IN 
select b.pole
	,data_type ,character_maximum_length
from (
	select 
		cast(json_array_elements(data)->>'pole' as varchar) pole 
		from (select p_pk as data) a 
	)b
	join information_schema.columns inf
	on inf.table_schema = p_table_schema
		and inf.table_name = p_table_name
		and inf.column_name = b.pole
LOOP
	if not right(x_sql,1)='(' then
		x_sql := x_sql||',';
	end if;		
	x_sql := x_sql||query_rec.pole||' '||query_rec.data_type;
	if query_rec.character_maximum_length is not null
	then
		x_sql := x_sql||'('||query_rec.character_maximum_length||')';
	end if;
end loop;
x_sql := x_sql||')';

--raise notice 'create table: %',x_sql;
execute x_sql;

--drop table mytemp;

--	update
	x_sql := 'with rows as (update '||p_table_schema||'.'||p_table_name||' tar set ';
	x_sql_sor:= 'select '||chr(10);
	-- příklad: 			cis_id = sor.cis_id::bigint,
	FOR  query_rec IN 
		select b.pole
		,data_type ,character_maximum_length
		from (
			select json_object_keys(data->0) pole
			from pp_data
		)b
		join information_schema.columns inf
		on inf.table_schema = p_table_schema
			and inf.table_name = p_table_name
			and inf.column_name = b.pole
	LOOP
		if not right(x_sql,5)=' set 'then
			x_sql := x_sql||', ';
			x_sql_sor:= x_sql_sor||',';
		end if;
		x_sql := x_sql||query_rec.pole||'=sor.'||query_rec.pole;
		x_sql_sor:= x_sql_sor||'cast(json_array_elements(data)->>'''||query_rec.pole||''' as '||query_rec.data_type||') '||query_rec.pole||chr(10);
	end loop;
	x_sql_sor:= x_sql_sor||' from pp_data a';
	-- auditní pole
	x_sql := x_sql||',update_batch_id = $1';
	x_sql := x_sql||',updated_at = now()';
	x_sql := x_sql||',updated_by = $2 ';
	-- from
	x_sql := x_sql||' from ('||x_sql_sor||') sor where '||x_sql_where;
	x_sql := x_sql||' RETURNING '||pole_pk_tar||') INSERT INTO mytemp ('||pole_pk||') SELECT '||pole_pk||' FROM rows';

if p_pk is not null 
then
--	raise notice 'p_pk: %',p_pk;
--	raise notice 'sql:  %',x_sql;
--	return;

	execute x_sql using p_batch_id,p_worker_name;

	x_sql:='select array_to_json(array_agg(row)) from (select '||pole_pk||' from mytemp) row';		
--	raise notice 'x_update: %',x_sql;
	execute x_sql into x_updated;
end if;

truncate table mytemp;
	
-- insert 
if p_pk is not null then
	x_sql:='with rows as (';
else
	x_sql:='';
end if;	
x_sql:=x_sql||'insert into '||p_table_schema||'.'||p_table_name||chr(10)||' ('||pole_seznam||',create_batch_id, created_at, created_by)'||chr(10);
x_sql:=x_sql||' select ';
--raise notice 'insert 1: %',x_sql;
-- select aa.cis_id::bigint
-- pole z json a datový typ
FOR  query_rec IN 
	select pole,isch.data_type
	from(
		select json_object_keys(data->0) pole
		from pp_data a 	
	) aa
	join information_schema.columns isch
	on
	     isch.table_name = p_table_name and table_schema =p_table_schema and upper(isch.column_name) =upper(aa.pole)
LOOP
	if pole_seznam_datatype is null then
		pole_seznam_datatype := chr(10)||'aa.'||query_rec.pole||'::'||query_rec.data_type;
	else
		pole_seznam_datatype := pole_seznam_datatype ||chr(10)||', aa.'||query_rec.pole||'::'||query_rec.data_type;
	end if;
end loop;
-- / pole z json a datový typ
x_sql:=x_sql||pole_seznam_datatype||chr(10);
-- auditní pole
	x_sql := x_sql||', $1 create_batch_id'||chr(10);
	x_sql := x_sql||',now() created_at'||chr(10);
	x_sql := x_sql||',$2 created_by'||chr(10);
x_sql := x_sql||' from ( select tar.*';
--raise notice 'insert 2: %',x_sql;
if p_sort is not null then
	x_sql:=x_sql||', row_number() over(partition by '||pole_pk_tar||' order by tar.'||pole_sort||') rn';
end if;
x_sql:=x_sql||' from ('||x_sql_sor||') tar'||chr(10);
if p_pk is not null then
	x_sql:=x_sql||' left join '||p_table_schema||'.'||p_table_name||' bb on '||chr(10);
	--raise notice 'insert 3: %',x_sql;

	prvni_pole := null;
	FOR  query_rec IN 
	select pole
	from (
		select 
			cast(json_array_elements(data)->>'pole' as varchar) pole 
			from (select p_pk as data) a 
		)b
	LOOP
		if prvni_pole is null then
			prvni_pole := query_rec.pole;
		end if;
		if not right(x_sql,5)=' on '||chr(10) then
			x_sql := x_sql||' and ';
		end if;		
		x_sql := x_sql||'tar.'||query_rec.pole||'= bb.'||query_rec.pole||chr(10);
	end loop;
end if;	
--raise notice 'insert 4: %',x_sql;
--raise notice 'prvni_pole: %',prvni_pole;
if p_pk is not null then
	--raise notice 'p_pk: %',p_pk;
	x_sql:=x_sql||' where bb.'||prvni_pole||' is null';
end if;
--raise notice 'insert 5: %',x_sql;
x_sql:=x_sql||') aa';
if p_sort is not null then
	x_sql:=x_sql||' where rn=1 '||chr(10);
end if;
if p_pk is not null then
	x_sql:=x_sql||' returning '||pole_pk||')'||chr(10);
	x_sql:=x_sql||' insert into mytemp ('||pole_pk||') select '||pole_pk||' from rows '||chr(10);
end if;	

--raise notice 'p_data: %',p_data;
--execute 'select '''||p_data||'''::json as data';
--raise notice 'insert 6: %',x_sql;
execute x_sql using p_batch_id,p_worker_name;
if p_pk is not null then
	x_sql:='select array_to_json(array_agg(row)) from (select '||pole_pk||' from mytemp) row';		
	execute x_sql into x_inserted;
end if;

						   
drop table mytemp;			

--raise EXCEPTION 'vše ok. %', x_sql_sor;				   
	
end;

$BODY$;

ALTER FUNCTION meta.import_general(bigint, json, character varying, character varying, json, json, character varying)
    OWNER TO postgres;

COMMENT ON FUNCTION meta.import_general(bigint, json, character varying, character varying, json, json, character varying)
    IS 'update/insert
parametry:
p_batch_id (bigint) --> číslo dávky
p_data (json) --> vstupní data (název_pole:hodnota)
p_table_schema (varchar) --> schéma cílové tabulky
p_table_name (varchar) --> název cílové tabulky
p_pk (json) --> primární klíč pro update a výběr insertu ("pole":nazev_pole)
p_sort (json) --> třídění při výběru ("pole":nazev_pole)
p_worker_name (varchar) --> název workeru (procesu/uživatele)
';
