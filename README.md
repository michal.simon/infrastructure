# Začínáme
[![Codeac.io](https://static.codeac.io/badges/3-13673668.svg "Codeac.io")](https://app.codeac.io/gitlab/operator-ict/golemio%2Fdevops%2Finfrastructure)

## Instalace
Postup jak [spustit celý projekt](docs/getting-started.md).

## Vizualizace
Jak napojit [vizualizační nástroje](docs/visualization.md) na data.

## Monitoring systému
Jak [funguje monitoring](docs/node-monitoring.md) a jaké nástroje jsou k němu dostupné.

# Release
[Vydání nové major verze API](docs/major-release.md)

# Infrastruktura
Představení [DEV/TEST infrastruktury](docs/infrastructure.md).

