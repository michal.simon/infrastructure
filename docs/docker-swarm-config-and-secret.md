## Jak aktualizovat config/secret v Docker Swarm?
Obě varianty, config i secret se aktualizují stejným principem. Oba typy souborů jsou immutable, tedy je potřeba založit nový config/secret a pak upravit docker service, aby používala ten nový. 

### Založení nového config/secret
Ve Swarmpitu (např http://10.172.0.30:888/configs/create) založit nový config.

![New config](img/docker-swarm-new-config.png)

### Aktualizace docker service
Rozkliknout detail service a kliknout na upravit.

![Update service](img/docker-swarm-edit-service.png)

A poté vybrat nový config a vše uložit.

![Update service](img/docker-swarm-update-service-config.png)

