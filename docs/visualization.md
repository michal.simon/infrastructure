#Visualization

## Grafana
### InfluxDB / Postgres
Grafana needs username/password for InfluxDB and PostgreSQL. So you have to create new users with right privileges and then fill them into Grafana datasources.

### MongoDB
Connection of MongoDB to Grafana requires proxy layer, for example [mongodb-grafana](https://github.com/JamesOsgood/mongodb-grafana). 

![Example of MongoDB connection with proxy](img/grafana-mongo-proxy.png)