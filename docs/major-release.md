# Release of new major version 


## 1. Create docker image v2
In the repository create a new branch and tag for current version (for example `v1`) and assume that the new version will be developed in `master` branch. 

Add new section into file `.gitlab-ci.yml`. In this section describe `build` and `registry_deployment` with the right new name of docker image. 
In following example for the `v1` version:

```
# Example
build_image_v1:
  stage: build
  script:
    - echo $CI_COMMIT_SHA > commitsha
    - docker build -t $CI_REGISTRY_IMAGE/v1/$CI_COMMIT_REF_NAME:$CI_PIPELINE_ID -t $CI_REGISTRY_IMAGE/v1/$CI_COMMIT_REF_NAME:latest .
  only:
    refs:
      - v1

registry_deployment_v1:
  stage: deploy
  script:
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
    - docker push $CI_REGISTRY_IMAGE/v1/$CI_COMMIT_REF_NAME:$CI_PIPELINE_ID && docker push $CI_REGISTRY_IMAGE/v1/$CI_COMMIT_REF_NAME:latest
  only:
    refs:
      - v1
```

Now we have separated docker images for each version.

## 2. Create new Docker service

Create new `docker service`, same as current service ([example for output-gateway](https://gitlab.oict.cz/data-platform/devops/infrastructure/blob/8fbce2a67445a6402731e3dbec2aa98a1957067b/gateways.yml#L114-146)). 
Just change the name of the service to include postfix with current version (example `output-gateway-v2`). All other services can call this service as `output-gateway-v2` (for example in routes of `permission-proxy`).

### Redis
Set a different number of Redis database than previous versions! To set a specific database number use the env `REDIS_CONN` with db number in the URL, e.g. to use db number `2` set url to `redis://localhost/2`.

## 3. Routing
In permission-proxy, get all current routes for current version and create the new ones (via API). New routes will contains `target_url: http://output-gateway-v2:3000`.

## 4. Database migrations
For the backward compatibility of current API you should do just nondestructive changes. When you stop obsolete API, is strictly recommended add migration for removal out dated structures and data.
