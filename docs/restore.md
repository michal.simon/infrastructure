# Grafana
Dle https://github.com/ysde/grafana-backup-tool
```
docker run --rm --name grafana-backup-tool \
   -e GRAFANA_TOKEN=eyJrIjoiRFROSldvZW5WOWxycmtmd0tTeWhtT2VVbGNMQTEwVHUiLCJuIjoiZ3JhZmFuYS1iYWNrdXAiLCJpZCI6MX0= \
   -e GRAFANA_URL=https://api.golemio.cz/grafana \
   -v /srv/backup/grafana:/backup  \
   --entrypoint ""\
   ysde/docker-grafana-backup-tool /opt/grafana-backup-tool/restore_grafana.sh /backup/2019-08-06T13:42:47.tar.gz
```
