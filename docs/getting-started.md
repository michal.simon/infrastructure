# Getting started

## Vocabulary

- machine = VM = virtual machine = server
- node = node of Docker Swarm cluster
- [manager node](https://docs.docker.com/engine/swarm/how-swarm-mode-works/nodes/#manager-nodes)
- [Docker service](https://docs.docker.com/get-started/part3/)
- [Docker stack](https://docs.docker.com/get-started/part5/)

## Prerequisites

1. Access to Docker registry with build Docker images.
1. Ssh access to all VM.
1. Already installed Docker Compose.
1. Already installed Docker Swarm. 
1. Installed Ansible on your localhost

## Creation of Docker Swarm cluster
Virtual machines (even in the case of 1 virtual machine) have to [connected](https://docs.docker.com/engine/swarm/swarm-tutorial/create-swarm/) into cluster. You should add docker labels to each docker node.

Labels restrict which nodes may be used for each Docker service. In case of single node cluster, add all labels to that node.

### Single machine example:
```
docker node update --label-add type-monitor=true my-vm
docker node update --label-add type-db=true my-vm
docker node update --label-add type-back=true my-vm
docker node update --label-add type-front=true my-vm
```

### 4-node cluster:
```
docker node update --label-add type-monitor=true my-vm1
docker node update --label-add type-db=true my-vm2
docker node update --label-add type-back=true my-vm3
docker node update --label-add type-front=true my-vm4
```

## Docker registry login
```
docker login https://registry.gitlab.com
```

## Create all Docker services

1. Duplicate `inventories/example.yml` to `inventories/my_project.yml`.
1. Fill configuration in `inventories/my_project.yml`.
1. Run `ansible-playbook -i inventories/my_project.yml playbooks/generate-environment.yml`. It prepares all files on cluster node of type monitor.
1. Login to remote server and call `base_path`/create-all.sh. It will create all docker stacks with their services. After that will works just some of services, because the rest is not configured yet.

State of current services is on `http://IP_ADRESS_OF_CLUSTER:888`. Username: admin, Password: admin.

## Setup relational databases
Use Adminer to login into PostgreSQL. Credentials are set in the file `database.yml` by constants `POSTGRES_USER` and `POSTGRES_PASSWORD`.

![Adminer login screen](img/adminer-postgres-login.png)

When you are logged in, create new databases for dataplatform (recommended: `dataplatform`) and for permission proxy (recommended: `permission_proxy`)

```
CREATE DATABASE dataplatform;
CREATE DATABASE permission_proxy;

CREATE EXTENSION postgis; -- activate extension postgis
```

Copy&paste SQL script from `init-database/init.sql` in the Adminer and change default values (usernames, passwoeds, ...). It creates users for modules with correct privileges. These credentials setup in file `gateways.yml`. 


## Database migrations
In Swarmpitu (`http://IP_ADDRESS_OF_CLUSTER:888`, `admin/admin`) go to detail of service `migration_schema-definitions-migration` (`http://IP_ADDRESS_OF_CLUSTER:888/services/migration_schema-definitions-migration`) and click on `Redeploy service`. It runs database migrations which prepare whole PostgreSQL database structure to you.

![redeploy migrations](img/swarmpit-redeploy-migrations.png)

Result will be seen in Adminer (`http://IP_ADDRESS_OF_CLUSTER:8080`).



# How run project on bare metal

## Prerequisites
1. A virtual machine with ssh access and root privileges.

## 3rd party software
For some the next software you have to create users with proper access rights and then setup it in each module. How to setup credentials is described in each module.

- [Node.js](https://nodejs.org/en/download/) 8+
- [RabbitMQ](https://www.rabbitmq.com/download.html)
- [Redis](https://redis.io/topics/quickstart) 5+
- [PostgreSQL](https://www.postgresql.org/docs/10/tutorial-install.html) 10.5+ and [PostGIS extension](https://postgis.net/install/)  2.5+
- [Mongo](https://docs.mongodb.com/manual/installation/) 4+
- [Flyway](https://flywaydb.org/getstarted/firststeps/commandline) - only for Permission proxy
- [InlfuxDB](https://docs.influxdata.com/influxdb/v1.7/introduction/installation/) - 1.7+, optional
- [Grafana](https://grafana.com/docs/installation/) - 5+, optional
- [Adminer](https://www.adminer.org/cs/) - optional, require PHP server

## Setup relational databases

- Same as `Setup relational databases` in Docker installation.
- Run [database migrations](https://gitlab.oict.cz/data-platform/code/schema-definitions/blob/development/docs/migrations.md#pomoc%C3%AD-npm)
- [Permission proxy](https://gitlab.oict.cz/data-platform/code/permission-proxy#installation).

## Module installation
Installation and setup of each module is describe in a repository of that module.
