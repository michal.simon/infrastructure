# Monitoring systému

## Logy
Each container has own ENV variable which define logging level. All logs are collect by [logspout](https://github.com/gliderlabs/logspout), then they are process by [Logstash](https://www.elastic.co/products/logstash) and save into [Elasticsearch](https://www.elastic.co/products/elasticsearch). For log exploration and dashboarding serve [Kibana](https://www.elastic.co/products/kibana).

> !!! All Kibana dashboards are stored in Elastic Search. !!!

Elasticsearch, Logstash a Kibana is called `ELK` stack.

Check lohstash pipeline.conf:
```
docker run -ti -v /srv/dp/elk/logstash/pipeline.conf:/logstash.conf:ro logstash:7.2.0 bash -c "logstash -t -f /logstash.conf"
```


## Metriky
Sběr metrik z jednotlivých stojů zajištuje [Telegraf](https://www.influxdata.com/time-series-platform/telegraf/). Ten odesílá definované metriky do [InfluxDB](https://docs.influxdata.com/influxdb/v1.7/). Pro nahlížení do surových dat a administraci uživatelů slouží [Chronograf](https://docs.influxdata.com/chronograf/v1.7/). Přes ten lze také nastavit uporozornění v [Kapacitoru](https://docs.influxdata.com/kapacitor/v1.5/). Telegraf, InfluxDB, Chronograf, Kapacitor se označují tako [TICK stack](https://www.influxdata.com/wp-content/uploads/InfluxDB_Diagram.png).

### První spuštení
Pro zprovoznění TICK stacku je třeba založit nového uživatele v Chronografu a jeho jméno a heslo vyplnit v konfiguračním souboru `telegraf.conf` v sekci `[[outputs.influxdb]]`.
